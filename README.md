# JSExample

Simple Node and React app to demonstrate an API based application.


## Author
Name: Andrew Adcock <andrewadcock@gmail.com>

Submission Date: 2021-09-14


## Usage
Clone the repository

`git clone https://gitlab.com/andrewadcock/jsexample.git`

CD in to the directory

`cd jsexample`

Add .env file in ${ROOT}/express. Copy .env-example to .env and modify as needed.

`cd express && cp .env-example .env`

Return to ${ROOT} and start containers
`cd ../ && docker-compose up`

Visit React site at [http://localhost:3000](http://localhost:3000)

Visit Express site at [http://localhost:5000](http://localhost:5000)

Visit Swagger Docs at [http://localhost:5000/api/v1/docs/](http://localhost:5000/api/v1/docs/) 

