const moment = require("moment");
const fs = require("fs");

/**
 * Logger Middleware
 *
 * Write log to file and console output
 */
const logger = async (req, res, next) => {

  const logEntry = `${moment().format()} | ${req.method} request: ${req.protocol}://${req.get('host')}${req.originalUrl} \n`

  const filePath  = __dirname + '/../logs/apiUsage.txt'

  // Create log file if it doesn't exist
  let fileReady = false
  try {
    if (!fs.existsSync(filePath)) {
      await fs.writeFile(filePath, "", (err) => {
        if(err)
          console.log('Error creating log file: ', err)
      })
    }
    fileReady = true
  } catch(err) {
    console.error("Could not read or write to filesystem:", err)
  }


  // If file exists write to it
  if(fileReady) {
    const writeStream = await fs.createWriteStream(filePath, {flags: 'a'})
    writeStream.on("open", () => {
      writeStream.write(logEntry, 'UTF8');
    })
  }

  console.log(logEntry);
  next()
}

module.exports = logger