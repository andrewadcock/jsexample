const express = require("express");
const logger = require("./middleware/logger");
const _ = require("lodash");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const bodyParser = require("body-parser");
const swaggerConfig = require("./swagger.json");

const app = express();
const PORT = process.env.PORT || 5000;
const API_VERSION_URL = "/api/v1";

// Init logger middleware
app.use(logger);

// Add cors support
app.use(
  cors({
    origin: "http://localhost:3000",
  })
);

// Participants
app.use(`${API_VERSION_URL}/participants`, require("./routes/participants"));

// Games
app.use(`${API_VERSION_URL}/games`, require("./routes/games"));

// Swagger Setup
// In the interest of time, this is setup as Proof of Concept only
app.use(
  `${API_VERSION_URL}/docs`,
  swaggerUi.serve,
  swaggerUi.setup(swaggerConfig)
);

app.listen(PORT, () => {
  console.log(`Express server listening at http://localhost:${PORT}`);
});
