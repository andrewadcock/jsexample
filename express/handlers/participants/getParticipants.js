const _ = require("lodash");
const participants = require("../../data/participants.json");
const cleanKeys = require("../../libs/cleanKeys");

const getParticipants = () => {
  // Convert object of objects to array of objects
  const participantsArray = Object.values(participants);

  // Tidy up key names
  const returnArray = cleanKeys(participantsArray);

  if (returnArray.length) return { items: returnArray, status_code: 200 };

  return { message: `Cannot find participants` };
};

module.exports = getParticipants;
