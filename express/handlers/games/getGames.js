const _ = require("lodash");
const games = require("../../data/games.json");
const cleanKeys = require("../../libs/cleanKeys");

const getGames = () => {
  // Convert object of objects to array of objects
  const gamesArray = Object.values(games);

  // Tidy up key names
  const returnArray = cleanKeys(gamesArray);

  for (const key in returnArray) {
    for (const subKey in returnArray[key]) {
      // Add correct spelling of loser and remove incorrect
      if (returnArray[key][subKey].looser_id) {
        returnArray[key][subKey].loser_id = returnArray[key][subKey].looser_id;
        delete returnArray[key][subKey].looser_id;
      }

      if (returnArray[key][subKey].looser_score) {
        returnArray[key][subKey].loser_score =
          returnArray[key][subKey].looser_score;
        delete returnArray[key][subKey].looser_score;
      }
    }
  }

  if (returnArray.length) return { items: returnArray, status_code: 200 };

  return { message: `Cannot find games` };
};

module.exports = getGames;
