const express = require("express");
const router = express.Router();
const getGames = require("../handlers/games/getGames");

router.get(`/`, (req, res) => {
  const games = getGames();
  res.status(games.status_code).json(games.items);
});

module.exports = router;
