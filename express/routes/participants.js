const express = require("express");
const router = express.Router();
const getParticipants = require("../handlers/participants/getParticipants");

// Get all participants
router.get(`/`, (req, res) => {
  const participants = getParticipants();
  res.status(participants.status_code).json(participants.items);
});

module.exports = router;
