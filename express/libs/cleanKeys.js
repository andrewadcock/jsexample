const cleanKeys = (array) => {
  let returnArray = array.map((participant) => {
    let cleanKeys = {};
    for (const key in participant) {
      // split on spaces
      let keyArray = key.split(" ");

      // Lowercase and replace spaces with underscores
      const cleanKey = keyArray
        .map((keyItem, index) => {
          return keyItem.toLowerCase();
        })
        .join("_");

      // Set to new array
      cleanKeys[cleanKey] = participant[key];
    }
    return cleanKeys;
  });

  return returnArray;
};

module.exports = cleanKeys;
