module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#242582",
        secondary: {
          DEFAULT: "#f64c72",
          dark: "#4d0213",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
