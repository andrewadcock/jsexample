import React from "react";
import "./App.css";
import ParticipantsInfo from "./components/components/participantsInfo/ParticipantsInfo";

function App() {
  return (
    <div className="App">
      <ParticipantsInfo />
    </div>
  );
}

export default App;
