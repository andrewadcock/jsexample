/**
 * Interface: ParticipantGames
 */

export default interface IParticipantGames {
  [key: string]: any;
}
