/**
 * Interface: ParticipantList
 */

export default interface IParticipantList {
  id: number;
  first_name: string;
  last_name: string;
}
