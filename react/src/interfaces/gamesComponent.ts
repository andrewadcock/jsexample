/**
 * Interface: GamessComponent
 */

export default interface IGamesComponent {
  games: any;
  participants: any;
  ownerId: number;
  retrieveParticipantInfo: Function;
}
