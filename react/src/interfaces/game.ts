/**
 * Interface: Game
 */

export default interface IGame {
  winner_score?: number;
  loser_id?: number;
  loser_score?: number;
}
