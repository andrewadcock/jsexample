/**
 * Interface: Participant
 */

export default interface IParticipant {
  image: string;
  name: string;
  ratio: string;
  [key: string]: any;
}
