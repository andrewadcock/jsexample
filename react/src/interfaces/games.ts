/**
 * Interface: Games
 */

import IGame from "./game";

export default interface IGames {
  [key: number]: IGame;
}
