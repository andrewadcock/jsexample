/**
 * Interface: ParticipantsComponent
 */

import IGames from "./games";
import IParticipantList from "./participantList";

export default interface IParticipantsComponent {
  participants: IParticipantList[] | undefined;
  games: IGames[] | undefined;
  updateSelected: Function;
  retrieveParticipantInfo: Function;
}
