/**
 * Interface: UserModalComponent
 */

import IParticipant from "./participant";

export default interface IUserModalComponent {
  participant: IParticipant;
  modalIsActive: Function;
}
