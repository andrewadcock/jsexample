import axios from "axios";

const retrieveParticipants = async () => {
  let items;
  await axios
    .get(`http://127.0.0.1:5000/api/v1/participants`)
    .then((response) => {
      items = response?.data;
    })
    .catch((err) => {
      console.log("Error retrieving participants data", err);
    });
  return items;
};

const retrieveGames = async () => {
  let items: any = [];
  await axios
    .get(`http://127.0.0.1:5000/api/v1/games`)
    .then((response) => {
      items = response?.data;
    })
    .catch((err) => {
      console.log("Error retrieving games data", err);
    });

  return items;
};

export { retrieveGames, retrieveParticipants };
