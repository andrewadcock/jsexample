const calculateWonGamesLength = (gameList: any, matchId: any) => {
  if (gameList) {
    let count = 0;
    for (const key in gameList) {
      if (gameList[key].loser_id !== matchId) count++;
    }
    return count;
  }
};

export { calculateWonGamesLength };
