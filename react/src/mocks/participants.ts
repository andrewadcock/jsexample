const participantsMock = [
  {
    id: 1,
    first_name: "Melody",
    last_name: "Rivera",
  },
  {
    id: 2,
    first_name: "Conrad",
    last_name: "Taylor",
  },
  {
    id: 3,
    first_name: "Katie",
    last_name: "Hinton",
  },
  {
    id: 4,
    first_name: "Roxy",
    last_name: "Yates",
  },
  {
    id: 5,
    first_name: "Jameel",
    last_name: "Orozco",
  },
  {
    id: 6,
    first_name: "Connie",
    last_name: "Kinney",
  },
  {
    id: 7,
    first_name: "Demi",
    last_name: "Stanton",
    banned: true,
  },
  {
    id: 8,
    first_name: "Mattie",
    last_name: "Parkes",
  },
  {
    id: 9,
    first_name: "Beatrice",
    last_name: "Paine",
  },
  {
    id: 10,
    first_name: "Adaline",
    last_name: "Winters",
  },
];
export default participantsMock;
