const gamesMock = {
  "0": {
    winner_score: 9,
    loser_id: 10,
    loser_score: 6,
  },
  "1": {
    winner_score: 9,
    loser_id: 7,
    loser_score: 5,
  },
  "2": {
    winner_score: 19,
    loser_id: 7,
    loser_score: 5,
  },
  "3": {
    winner_score: 11,
    loser_id: 9,
    loser_score: 5,
  },
};

export default gamesMock;
