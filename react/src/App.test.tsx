import React from "react";
import App from "./App";
import { shallow } from "enzyme";
import ParticipantsInfo from "./components/components/participantsInfo/ParticipantsInfo";

describe("Renders participantsInfo component", () => {
  let wrapper = shallow(<App />);

  test("Check that page title shows", () => {
    expect(wrapper.containsMatchingElement(<ParticipantsInfo />)).toEqual(true);
  });
});
