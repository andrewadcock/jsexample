import { shallow } from "enzyme";
import React from "react";
import ParticipantsInfo from "./ParticipantsInfo";
import Games from "../games/Games";
import Participants from "../participants/Participants";

describe("Renders participantInfo component", () => {
  let wrapper = shallow(<ParticipantsInfo />);

  test("Check that all sub components render", () => {
    expect(wrapper.containsMatchingElement(<Games />)).toEqual(true);
    expect(wrapper.containsMatchingElement(<Participants />)).toEqual(true);
  });

  test("Check header content", () => {
    expect(wrapper.find("h3").text()).toEqual("Participant Info");
  });
});
