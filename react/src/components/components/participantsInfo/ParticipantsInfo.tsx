import React, { useEffect, useState } from "react";
import Participants from "../participants/Participants";
import Games from "../games/Games";
import UserModal from "../modals/UserModal";
import { calculateWonGamesLength } from "../../../helpers/helperFunctions";
import { retrieveParticipants, retrieveGames } from "../../../helpers/api";
import IParticipantList from "../../../interfaces/participantList";
import IGames from "../../../interfaces/games";
import IParticipant from "../../../interfaces/participant";
import IParticipantGames from "../../../interfaces/participantGames";

const ParticipantsInfo = () => {
  const [currentParticipant, setCurrentParticipant] = useState<number>(-1);
  const [participantList, setParticipantList] = useState<IParticipantList[]>();
  const [participantGames, setParticipantGames] = useState<IParticipantGames>();
  const [games, setGames] = useState<IGames[]>();

  const [modalActive, setModalActive] = useState<boolean>(true);
  const [modalParticipant, setModalParticipant] = useState<IParticipant>();

  useEffect(() => {
    // Get and set to local state, Games, and Participants
    const getData = async () => {
      setParticipantList(await retrieveParticipants());
      setGames(await retrieveGames());
    };

    getData().then();
  }, []);

  // set ParticipantGame when participant is selected
  useEffect(() => {
    if (games && games[currentParticipant - 1]) {
      setParticipantGames(games[currentParticipant - 1]);
    }
  }, [currentParticipant, games]);

  const updateSelected = (selected: number) => {
    if (selected) setCurrentParticipant(selected);
  };
  const modalIsActive = (value: boolean) => {
    setModalActive(value);
  };

  // Calculate and set
  const retrieveParticipantInfo = (participantId: number) => {
    // adjust participant id to match games ids
    const participantGameIndex = participantId + 1;

    // Determine won games
    let wonGames;
    if (games && games[participantGameIndex]) {
      wonGames = calculateWonGamesLength(
        games[participantGameIndex - 1],
        participantGameIndex
      );
    }

    // initialize data object
    let data: any = {};
    data.ratio = "0/0";
    if (participantList && participantList[participantId]) {
      if (games && games[participantGameIndex - 1]) {
        data.ratio = `${Object.keys(games[participantGameIndex - 1]).length}/${
          wonGames ?? 0
        }`;
      }
      data.name = `${participantList[participantId].first_name} ${participantList[participantId].last_name}`;
      data.image = `https://www.monteirolobato.edu.br/public/assets/admin/images/avatars/avatar${
        participantId + 1
      }_big.png`;
    }

    setModalParticipant(data);
    setModalActive(true);
  };

  return (
    <div className="container">
      <h3>Participant Info</h3>
      <div className="flex">
        <div className="flex-auto border-2 border-gray-100">
          <Participants
            participants={participantList}
            games={games}
            updateSelected={updateSelected}
            retrieveParticipantInfo={retrieveParticipantInfo}
          />
        </div>
        <div className="flex-auto border-2 border-gray-100">
          <Games
            participants={participantList}
            games={participantGames}
            ownerId={currentParticipant}
            retrieveParticipantInfo={retrieveParticipantInfo}
          />
        </div>
        {modalActive && modalParticipant ? (
          <UserModal
            participant={modalParticipant}
            modalIsActive={modalIsActive}
          />
        ) : null}
      </div>
    </div>
  );
};

export default ParticipantsInfo;
