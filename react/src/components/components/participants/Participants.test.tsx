import { shallow } from "enzyme";
import participantsMock from "../../../mocks/participants";
import gamesMock from "../../../mocks/games";
import Participants from "./Participants";
import React from "react";

describe("Renders participants component", () => {
  const updateSelected = jest.fn();
  const retrieveParticipantInfo = jest.fn();

  let wrapper = shallow(
    <Participants
      participants={participantsMock}
      games={gamesMock}
      updateSelected={updateSelected}
      retrieveParticipantInfo={retrieveParticipantInfo}
    />
  );

  test("Check number of participants", () => {
    // Add one to account for header
    expect(wrapper.find("tbody > tr")).toHaveLength(10);
    expect(wrapper.find("tbody > tr > td")).toHaveLength(30);
  });

  test("Check table contents", () => {
    const firstRowColumns = wrapper.find("td").map((col) => col.text());

    // Row One
    expect(firstRowColumns[0]).toEqual("Melody Rivera");
    expect(firstRowColumns[1]).toEqual("3/3");
    expect(firstRowColumns[2]).toEqual("Select");
  });
});
