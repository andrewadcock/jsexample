import React, { useState } from "react";
import IParticipantsComponent from "../../../interfaces/participantsComponent";
import { calculateWonGamesLength } from "../../../helpers/helperFunctions";

const Participants = ({
  participants,
  games,
  updateSelected,
  retrieveParticipantInfo,
}: IParticipantsComponent) => {
  const [currentSelected, setCurrentSelected] = useState<number[]>();

  const handleSelection = (participantId: number) => {
    updateSelected(participantId);
    setCurrentSelected([participantId]);
  };

  let displayParticipants;

  if (participants) {
    displayParticipants = participants.map(
      (participant: any, index: number) => {
        let ratio = "0/0";
        if (games) {
          // Subtract one to match game keys
          let participantGames = games[participant.id - 1];

          let count;
          let won;
          if (participantGames) {
            count = Object.keys(participantGames).length;

            // Filter if participant is also loser
            won = calculateWonGamesLength(participantGames, participant.id);

            // Set ratio string
            ratio = `${count}/${won}`;
          }
        }

        return (
          <tr
            key={index}
            className={
              currentSelected && currentSelected[0] === participant.id
                ? "selected"
                : ""
            }
          >
            <td>
              <div
                onClick={() => retrieveParticipantInfo(participant.id - 1)}
              >{`${participant.first_name} ${participant.last_name}`}</div>
            </td>
            <td>{ratio}</td>
            <td>
              <button onClick={() => handleSelection(participant.id)}>
                Select
              </button>
            </td>
          </tr>
        );
      }
    );
  }

  return (
    <>
      <h3>Participants</h3>
      <table>
        <thead>
          <tr>
            <th>the name</th>
            <th>played/won</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{displayParticipants}</tbody>
      </table>
    </>
  );
};

export default Participants;
