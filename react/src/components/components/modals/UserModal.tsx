import IUserModalComponent from "../../../interfaces/userModalComponent";

const UserModal = ({ participant, modalIsActive }: IUserModalComponent) => {
  return (
    <div className="user-modal">
      <div className="container border-4 border-gray-100 p-4">
        {participant ? (
          <>
            <button
              onClick={() => modalIsActive(false)}
              className="text-secondary float-right hover:text-secondary-dark"
            >
              [x] <span className="text-xs">Close</span>
            </button>
            <div className="clear-both">
              <img
                alt={participant.name}
                src={participant.image}
                className="my-8 m-auto"
              />
              <h3 className="text-primary font-bold">{participant.name}</h3>
              <div data-testid="ratio">Played/Won: {participant.ratio}</div>
            </div>
          </>
        ) : (
          <p>No data provided.</p>
        )}
      </div>
    </div>
  );
};

export default UserModal;
