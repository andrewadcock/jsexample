import { shallow } from "enzyme";
import UserModal from "./UserModal";
import React from "react";

describe("Renders Games component", () => {
  const modalParticipant1 = {
    ratio: "3/3",
    name: "Melody Rivera",
    image:
      "https://www.monteirolobato.edu.br/public/assets/admin/images/avatars/avatar1_big.png",
  };

  const modalParticipant2 = {
    ratio: "4/2",
    name: "Captain Tucker",
    image:
      "https://www.monteirolobato.edu.br/public/assets/admin/images/avatars/avatar4_big.png",
  };

  const modalIsActive = jest.fn();

  let wrapper = shallow(
    <UserModal participant={modalParticipant1} modalIsActive={modalIsActive} />
  );

  test("Check for avatar", () => {
    expect(wrapper.find("img")).toHaveLength(1);
  });

  test("Check for ratio", () => {
    expect(wrapper.find("h3").text()).toContain("Melody Rivera");
  });

  test("Check for ratio", () => {
    expect(wrapper.find({ "data-testid": "ratio" }).text()).toContain(
      "Played/Won: 3/3"
    );
  });

  let wrapper2 = shallow(
    <UserModal participant={modalParticipant2} modalIsActive={modalIsActive} />
  );

  test("Check for avatar", () => {
    expect(wrapper2.find("img")).toHaveLength(1);
  });

  test("Check for ratio", () => {
    expect(wrapper2.find("h3").text()).toContain("Captain Tucker");
  });

  test("Check for ratio", () => {
    expect(wrapper2.find({ "data-testid": "ratio" }).text()).toContain(
      "Played/Won: 4/2"
    );
  });
});
