import React, { Fragment } from "react";
import IParticipant from "../../../interfaces/participant";
import IGamesComponent from "../../../interfaces/gamesComponent";

const Games = ({
  games,
  participants,
  ownerId,
  retrieveParticipantInfo,
}: IGamesComponent) => {
  let displayGames: any = [];
  if (games) {
    displayGames = [];

    for (const i of Object.keys(games)) {
      let winner = { name: "Unknown", id: -1 };

      // Loop through all object and determine winner
      if (Object.keys(games[i]).length && games[i].loser_id !== ownerId)
        winner = {
          id: ownerId - 1,
          name: `${participants[ownerId - 1].first_name} ${
            participants[ownerId - 1].last_name
          }`,
        };

      // Find loser
      const loserObject = participants.filter(
        (participant: IParticipant) => games[i].loser_id === participant.id
      );

      let loser = { name: "Unknown", id: -1 };
      if (loserObject) {
        loser.name = `${loserObject[0].first_name} ${loserObject[0].last_name}`;
        loser.id = loserObject[0].id;
      }

      displayGames.push(
        <Fragment key={i}>
          <tr>
            <td>
              <div
                onClick={() =>
                  winner.name !== "Unknown"
                    ? retrieveParticipantInfo(winner.id)
                    : null
                }
              >
                {winner.name}
              </div>
            </td>
            <td>
              <div
                onClick={() =>
                  loser.name !== "Unknown"
                    ? retrieveParticipantInfo(loser.id - 1)
                    : null
                }
              >
                {loser.name}
              </div>
            </td>
          </tr>
          <tr>
            <td>{games[i].winner_score}</td>
            <td>{games[i].loser_score} </td>
          </tr>
        </Fragment>
      );
    }
  }

  return (
    <>
      <h3>Games Played</h3>
      {displayGames && Object.keys(displayGames).length ? (
        <table>
          <tbody>{displayGames}</tbody>
        </table>
      ) : (
        <p>No games found</p>
      )}
    </>
  );
};

export default Games;
