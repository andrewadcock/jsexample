import { shallow } from "enzyme";
import Games from "./Games";
import participantsMock from "../../../mocks/participants";
import gamesMock from "../../../mocks/games";

describe("Renders Games component", () => {
  const participantList = participantsMock;
  const participantGames = gamesMock;
  const currentParticipant = 1;
  const retrieveParticipantInfo = jest.fn();

  let wrapper = shallow(
    <Games
      participants={participantList}
      games={participantGames}
      ownerId={currentParticipant}
      retrieveParticipantInfo={retrieveParticipantInfo}
    />
  );

  test("Check number of games", () => {
    expect(wrapper.find("tr")).toHaveLength(8);
    expect(wrapper.find("td")).toHaveLength(16);
  });

  test("Check table contents", () => {
    const firstRowColumns = wrapper.find("td").map((col) => col.text());

    expect(firstRowColumns[0]).toEqual("Melody Rivera");
    expect(firstRowColumns[1]).toEqual("Adaline Winters");
    expect(firstRowColumns[2]).toEqual("9");
    expect(firstRowColumns[3]).toEqual("6 ");
  });
});
